const Helpers = {
  htmlCollectionToArray: (htmlC) => {
    const arr = [];
    for (let i = 0; i < htmlC.length; i++) {
      arr.push(htmlC.item(i));
    }
    return arr;
  },
  getByClass: (classname) => {
    return Helpers.htmlCollectionToArray(
      document.getElementsByClassName(classname)
    );
  },
  attachOnloadListener: (listener) => {
    document.addEventListener("onload", listener);
  },
};

const redditOnloadListener = () => {
  const url = document.URL;
  const REDIRECTS = ["popular", "all"];

  const needsRedirect = !!REDIRECTS.find((r) => {
    return url.endsWith(`/r/${r}/`) || url.startsWith(`https://${r}.`);
  });

  if (needsRedirect) {
    window.location.replace("https://www.reddit.com/");
  }
};

Helpers.attachOnloadListener(redditOnloadListener);

const Reddit = () => {
  const choiceTextSearch = (choices, text) => {
    return choices.find((c) => {
      return c.text.toLowerCase() == text;
    });
  };

  const HIDE_CHOICES = ["popular", "all", "random"];

  return {
    main: () => {
      redditOnloadListener();

      const topChoices = Helpers.getByClass("choice");

      HIDE_CHOICES.forEach((hc) => {
        const elem = choiceTextSearch(topChoices, hc);
        if (!!elem) {
          elem.parentElement.remove();
        }
      });
    },
  };
};

(() => {
  Reddit().main();
})();
