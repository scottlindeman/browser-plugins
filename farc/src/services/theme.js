import StorageClient from "../clients/storage.js";

// --- Constants ---------------------------------

const Palettes = {
  gruvbox_light: [
    "#282828",
    "#F9F5D7",
    "#EBD9B3",
    "#9D0006",
    "#AF3A03",
    "#928374",
    "#EBD9B3",
    "#79740E",
  ],
  gruvbox_dark: [
    "#fbf1c7",
    "#32302f",
    "#A49580",
    "#fb4934",
    "#fe8019",
    "#fabd2f",
    "#83a598",
    "#b8bb26",
  ],
  zenburn_light: [
    "#3F3F3F",
    "#DCDCCC",
    "#989890",
    "#DFAF8F",
    "#6CA0A3",
    "#F0DFAF",
    "#9FC59F",
    "#CC9393",
  ],
  zenburn_dark: [
    "#dcdccc",
    "#3f3f3f",
    "#6f6f6f",
    "#dfaf8f",
    "#f0dfaf",
    "#bde0f3",
    "#7f9f7f",
    "#cc9393",
  ],
  rose_pine_light: [
    "#5F5B7F",
    "#FAF4ED",
    "#907AA9",
    "#B4637A",
    "#286983",
    "#56959F",
    "#D7827E",
    "#EA9D34",
  ],
  rose_pine_dark: [
    "#E0DEF4",
    "#232136",
    "#C4A7E7",
    "#EA9A97",
    "#9CCFD8",
    "#3E8FB0",
    "#EB6F92",
    "#F6C177",
  ],
  everforest_light: [
    "#5C6A72",
    "#F8F0DC",
    "#999F93",
    "#DF69BA",
    "#DFA000",
    "#E66868",
    "#E6E9C4",
    "#8DA101",
  ],
  everforest_dark: [
    "#D3C6AA",
    "#323D43",
    "#858C7F",
    "#D699B6",
    "#DBBC7F",
    "#E67E80",
    "#5D4251",
    "#A7C080",
  ],
  ayu_light: [
    "#5C6166",
    "#FCFCFC",
    "#D3E1F5",
    "#399EE6",
    "#FA8D3E",
    "#86B300",
    "#D3E1F5",
    "#E6BA7E",
  ],
  ayu_dark: [
    "#CCCAC2",
    "#242936",
    "#274364",
    "#73D0FF",
    "#FFAD66",
    "#D5FF80",
    "#274364",
    "#95E6CB",
  ],
  catppuccin_light: [
    "#4C4F69",
    "#EFF1F5",
    "#CCD0DA",
    "#E64553",
    "#40A02B",
    "#DC8A78",
    "#9CA0B0",
    "#7287FD",
  ],
  catppuccin_dark: [
    "#CAD3F5",
    "#24273A",
    "#A5ADCB",
    "#EE99A0",
    "#F4DBD6",
    "#EED49F",
    "#6E738D",
    "#A6D189",
  ],
  solarized_light: [
    "#586E75",
    "#FDF6E3",
    "#EEE8D5",
    "#DC322F",
    "#268BD2",
    "#859900",
    "#93A1A1",
    "#B58900",
  ],
  solarized_dark: [
    "#93A1A1",
    "#002B36",
    "#EEE8D5",
    "#DC322F",
    "#268BD2",
    "#859900",
    "#586E75",
    "#B58900",
  ],
};

const DEFAULT = Palettes.gruvbox_dark;

// --- Service -----------------------------------

class _ThemeService {
  get palettes() {
    return Palettes;
  }

  /**
   * Returns the current theme in storage, or else the default one.
   * @return {Array[String]} The list of colors
   */
  async getCurrentTheme() {
    const colors = await StorageClient.getUITheme();

    return colors.get(DEFAULT);
  }

  /**
   * Saves a new theme to local storage
   * @param {Array[String]} The new array of hex colors
   */
  async setNewTheme(theme) {
    await StorageClient.setUITheme(theme);
  }

  /**
   * Update the innerHTML of the sidebar to use the given theme.
   * @param {Window} The window to apply the themes to
   * @param {Array[String]} The list of hex colors
   */
  updateSidebarCSS(selectedDocument, theme) {
    const headStyle = selectedDocument.head.getElementsByTagName("style")[0];

    headStyle.innerHTML =
      ":root {\n" +
      `  --custom-foreground: ${theme[0]};\n` +
      `  --custom-background: ${theme[1]};\n` +
      `  --custom-highlight:  ${theme[2]};\n` +
      `  --custom-critical:   ${theme[3]};\n` +
      `  --custom-salient:    ${theme[4]};\n` +
      `  --custom-popout:     ${theme[5]};\n` +
      `  --custom-subtle:     ${theme[6]};\n` +
      `  --custom-faded:      ${theme[7]};\n` +
      "}";
  }

  /**
   * Combines getting the current theme and updating the CSS.
   * Generally use this when loading the sidebar for the first time.
   */
  async load() {
    const theme = await this.getCurrentTheme();
    this.updateSidebarCSS(document, theme);
  }
}

const ThemeService = new _ThemeService();

// --- Exports -----------------------------------

export default ThemeService;
