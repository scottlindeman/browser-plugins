/**
 * A general function for ensuring all fields
 * exist in the object being passed in and passing back
 */
const objParser = (fields, obj) => {
  const extractedObj = {};
  fields.forEach((f) => {
    if (typeof f === "string") {
      if (obj.hasOwnProperty(f)) {
        extractedObj[f] = obj[f];
      } else {
        throw new Error(`Missing scaffold key: ${f}`);
      }
    } else if (typeof f === "object") {
      // This should be a single object mapping of key to Scaffold
      const key = Object.keys(f)[0];
      if (obj.hasOwnProperty(key)) {
        extractedObj[key] = objParser(f[key].fields, obj);
      } else {
        throw new Error(`Missing scaffold key: ${key}`);
      }
    } else {
      throw new Error(`Unexpected field type: ${typeof f}`);
    }
  });
  return extractedObj;
};

const Scaffold = (...fields) => {
  return {
    fields: fields,
    parseAttrs: (vnode) => {
      return objParser(fields, vnode.attrs);
    },
    parseState: (vnode) => {
      return objParser(fields, vnode.state);
    },
    parse: (obj) => {
      return objParser(fields, obj);
    },
  };
};

// --- Exports -----------------------------------

export default Scaffold;
