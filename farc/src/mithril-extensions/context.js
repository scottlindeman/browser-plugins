import { Frozen } from "../common.js";

// --- Base --------------------------------------

/**
 * This is an implementation of passing state and actions
 * down through the component chain. Logic on modifying a vnode's
 * state can go here and make the components themselves.
 *
 * It's important that consumers of the context don't have access
 * to the vnode state. Unfortunately subclasses cannot inherit
 * private fields from parent classes, so we are just putting
 * an underscore.
 **/
class MithrilContext extends Frozen {
  constructor(vnode) {
    super({ _vnode: vnode });
  }
}

export default MithrilContext;
