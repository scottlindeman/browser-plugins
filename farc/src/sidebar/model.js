import { Frozen, NoneOption, SomeOption, Logger } from "../common.js";
import FarcConstants from "../constants.js";

const LOGGER = new Logger("Model");

// --- Tabs --------------------------------------

class FarcTab extends Frozen {
  /**
   * @param {Option[browser.tabs.Tab]} browserTab The actual associated tab in the browser
   * @param {Object} obj Additional parameters from subclasses
   */
  constructor(browserTab, obj, defaultTitle = "", defaultUrl = "") {
    const params = { browserTab, defaultTitle, defaultUrl };
    Object.assign(params, obj);

    super(params);
  }

  /**
   * The browserTabId, if it exists.
   * @return {Option[Integer]}
   */
  get tabId() {
    return this.browserTab.map((t) => {
      return t.id;
    });
  }

  /**
   * The title of the tab.
   * @return {String}
   */
  get title() {
    return this.browserTab
      .map((t) => {
        const isInternalTitle =
          Object.values(FarcConstants.PinnedTabType).indexOf(t.title) >= 0;
        if (isInternalTitle) {
          return this.defaultTitle;
        } else {
          return t.title;
        }
      })
      .get(this.defaultTitle);
  }

  /**
   * The url of the tab.
   * @return {String}
   */
  get url() {
    return this.browserTab
      .map((t) => {
        return t.url;
      })
      .get(this.defaultUrl);
  }

  /**
   * Whether the tab is muted.
   * @return {Boolean}
   */
  get isMuted() {
    return this.browserTab
      .map((t) => {
        return t.muted;
      })
      .get(false);
  }

  /**
   * Whether the tab has a push notification
   * @return {Boolean}
   */
  get hasNotification() {
    return this.browserTab
      .map((t) => {
        return t.attention;
      })
      .get(false);
  }

  /**
   * If there is favicon data, it will be returned.
   * @return {String}
   */
  get favIconData() {
    const url = this.browserTab
      .map((t) => {
        if (t.favIconUrl === undefined || t.favIconUrl === "") {
          return this.url;
        }
        return t.favIconUrl;
      })
      .get(this.url);
    return url;
  }

  /**
   * Given maybe a farcTab, determine whether this is the active tab.
   * @param {Option[FarcTab]} maybeFarcTab A potential farcTab to check against
   * @return {Boolean}
   */
  isActive(maybeFarcTab) {
    return maybeFarcTab
      .map((ft) => {
        if (this.tabId.isEmpty() || ft.tabId.isEmpty()) {
          return false;
        } else {
          return this.tabId.get() === ft.tabId.get();
        }
      })
      .get(false);
  }

  /**
   * Get the unique accordionId for this tab
   */
  get accId() {
    return `farc-accordion-${this.tabId.get()}`;
  }
}

class FarcPinnedTab extends FarcTab {
  /**
   * A Pinned Tab. This is different from a brower's definition of a pinned tab, mostly
   * that this tab will always remember it's base url, and always be visible, even if there's
   * no actual backing browserTab.
   * @param {Option[browser.tabs.Tab]} browserTab The associated tab in the browser, if it exists.
   * @param {browser.bookmarks.BookmarkTreeNode} browserBookmarkNode The backing browser bookmark node.
   * @param {String} pinnedTabType One of FarcConstants.PinnedTabType
   */
  constructor(browserTab, browserBookmarkNode, pinnedTabType) {
    super(
      browserTab,
      { browserBookmarkNode, pinnedTabType },
      browserBookmarkNode.title,
      browserBookmarkNode.url
    );
  }

  get bookmarkId() {
    return this.browserBookmarkNode.id;
  }

  get accId() {
    return `farc-accordion-${this.bookmarkId}`;
  }

  /**
   * Create a pinned tab from just a bookmark node.
   * @param {browser.bookmarks.BookmarkTreeNode} browserBookmarkNode
   * @return {FarcPinnedTab}
   */
  static fromBookmarkNode(browserBookmarkNode, pinnedTabType) {
    return new FarcPinnedTab(NoneOption, browserBookmarkNode, pinnedTabType);
  }

  /**
   * Create a new version of this pinned tab, but with an actual browser tab
   * @param {browser.tabs.Tab} browserTab A real browser tab
   * @return {FarcPinnedTab}
   */
  withBrowserTab(browserTab) {
    return new FarcPinnedTab(
      SomeOption(browserTab),
      this.browserBookmarkNode,
      this.pinnedTabType
    );
  }

  /**
   * Create a new version of this pinned tab, with without a browser tab
   * @return {FarcPinnedTab}
   */
  removeBrowserTab() {
    return new FarcPinnedTab(
      NoneOption,
      this.browserBookmarkNode,
      this.pinnedTabType
    );
  }
}

class FarcFreeTab extends FarcTab {
  /**
   * Free tabs can only be created with a real associated browser tab.
   * @param {browser.tabs.Tab} browserTab The real tab
   */
  constructor(browserTab) {
    super(SomeOption(browserTab), {});
  }
}

// --- Spaces ------------------------------------

class FarcSpaceLite extends Frozen {
  constructor(title, bookmarkId, node) {
    super({ title, bookmarkId, node });
  }

  static fromBookmarkNode(node) {
    return new FarcSpaceLite(node.title, node.id, node);
  }
}

class FarcSpaceHeavy extends Frozen {
  constructor(bookmarkId, title, pinnedTabs, freeTabs) {
    super({ bookmarkId, title, pinnedTabs, freeTabs });
  }

  findAndUpdatePinnedTab(tab) {
    const idx = this.pinnedTabs.findIndex((pt) => {
      return pt.url === tab.url;
    });

    if (idx < 0) {
      LOGGER.error(`Pinned tab with url "${tab.url}" not found.`);
      return;
    }
    const oldTab = this.pinnedTabs[idx];
    const newTab = oldTab.withBrowserTab(tab);

    this.pinnedTabs[idx] = newTab;
    return newTab;
  }

  findAndRemovePinnedTab(tab) {
    const idx = this.pinnedTabs.findIndex((pt) => {
      if (pt.tabId.isEmpty()) {
        return false;
      } else {
        return pt.tabId.get() === tab.id;
      }
    });

    if (idx < 0) {
      LOGGER.error(`Pinned tab with id "${tab.id}" not found.`);
      return false;
    }

    const oldTab = this.pinnedTabs[idx];
    const newTab = oldTab.removeBrowserTab();

    this.pinnedTabs[idx] = newTab;
    return true;
  }

  addFreeTab(tab) {
    const newFarcTab = new FarcFreeTab(tab);
    this.freeTabs.push(newFarcTab);
    return newFarcTab;
  }

  removeFreeTab(tab) {
    const idx = this.freeTabs.findIndex((ft) => {
      return ft.tabId.get() === tab.id;
    });
    if (idx < 0) {
      LOGGER.error(`Tab with id ${tab.id} not found in freetabs`);
      return false;
    }
    this.freeTabs.splice(idx, 1);
    return true;
  }

  updateFreeTab(tab) {
    const idx = this.freeTabs.findIndex((ft) => {
      return ft.tabId.get() === tab.id;
    });

    if (idx < 0) {
      LOGGER.error(`Tab with id ${tab.id} not found in freetabs`);
      return;
    }

    const oldTab = this.freeTabs[idx];

    const newTab = new FarcFreeTab(tab);

    this.freeTabs[idx] = newTab;
  }
}

// --- Sidebar -----------------------------------

class FarcSidebar extends Frozen {
  constructor(top, spaces, currentSpace) {
    super({ top, spaces, currentSpace });
  }
}

// --- Listener Holders --------------------------

class FarcSpaceEventListeners extends Frozen {
  constructor(
    tabOnCreate,
    tabOnRemove,
    tabOnUpdate,
    bookmarkOnUpdate,
    bookmarkOnCreate,
    bookmarkOnRemove
  ) {
    super({
      tabOnCreate,
      tabOnRemove,
      tabOnUpdate,
      bookmarkOnUpdate,
      bookmarkOnCreate,
      bookmarkOnRemove,
    });
  }
}

// --- Entrypoint --------------------------------

export {
  FarcPinnedTab,
  FarcFreeTab,
  FarcSpaceLite,
  FarcSpaceHeavy,
  FarcSidebar,
  FarcSpaceEventListeners,
};
