import { NoneOption, SomeOption, Logger } from "../../common.js";
import FarcConstants from "../../constants.js";

import BookmarkClient from "../../clients/bookmarks.js";
import StorageClient from "../../clients/storage.js";
import TabClient from "../../clients/tabs.js";

import ThemeService from "../../services/theme.js";

import { FarcSpaceLite, FarcSidebar } from "../model.js";

import { bookmarkNodeMapper } from "./helpers.js";

const LOGGER = new Logger("SidebarService");

// --- Service -----------------------------------

class _SidebarService {
  /**
   * Load the list of top pinned tabs for the sidebar.
   * @param {Array[browser.bookmarks.BrowserBookmarkNode]} topnodes The bookmark nodes.
   * @return {Array[PinnedFarcTab]}
   */
  async #getTopTabs(topnodes) {
    const topbts = await StorageClient.listTopTabs();

    const farcTopTabs = await bookmarkNodeMapper(
      topnodes.pinned,
      topbts,
      FarcConstants.PinnedTabType.TOP
    );

    if (farcTopTabs.length === 0) {
      LOGGER.info("No top tabs defined");
    }

    return farcTopTabs;
  }

  /**
   * List all spaces from bookmark nodes.
   * @param {Array[browser.bookmarks.BrowserBookmarkNode]} spacesNodes The list of nodes mapping to spaces
   * @return {Array[FarcSpaceLite]}
   */
  #getSpaces(spacesNodes) {
    return spacesNodes.map((s) => {
      return FarcSpaceLite.fromBookmarkNode(s);
    });
  }

  /**
   * Come up with the first space to load on start.
   * @param {Array[FarcSpaceLite]} spaceLites The spaces to choose from
   * @return FarcSpaceLite
   */
  async #getCurrentSpace(spaceLites) {
    const currentIdx = await StorageClient.getCurrentSpace();

    return currentIdx
      .map((v) => {
        const found = spaceLites.find((l) => {
          return l.node.id === v;
        });
        return found === undefined ? NoneOption : SomeOption(found);
      })
      .get(NoneOption)
      .get(spaceLites[0]); // TODO: We get into a little trouble if there are no spaces at all.
  }

  /**
   * Grab the initial state of tabs from bookmarks and storage.
   * @return {FarcSidebar}
   */
  async load() {
    await ThemeService.load();
    const farcnode = await BookmarkClient.getOrCreateTop();
    const topnodes = await BookmarkClient.getChildrenTop(farcnode);

    const farcTopTabs = await this.#getTopTabs(topnodes);
    const spaces = this.#getSpaces(topnodes.spaces);
    const currentSpace = await this.#getCurrentSpace(spaces);

    return new FarcSidebar(farcTopTabs, spaces, currentSpace);
  }

  addSidebarEventListeners(vnode, redraw) {
    const uListener = BookmarkClient.onUpdate(
      async (bookmarkId, changeInfo) => {
        // Add check if the bookmarkId is a Space or TopNode?
        const newSidebar = await this.load();
        vnode.state.childrenNodes = newSidebar.top;
        vnode.state.spaces = newSidebar.spaces;
        vnode.state.currentSpace = newSidebar.currentSpace;
        redraw();
      }
    );
    const cListener = BookmarkClient.onCreate(async (bookmarkId, treeNode) => {
      const newSidebar = await this.load();
      vnode.state.childrenNodes = newSidebar.top;
      vnode.state.spaces = newSidebar.spaces;

      const isTopLevel =
        treeNode.parentId === vnode.state.currentSpace.node.parentId;
      const isSpace = treeNode.type === "folder";
      if (isTopLevel && isSpace) {
        vnode.state.currentSpace = FarcSpaceLite.fromBookmarkNode(treeNode);
      }
      redraw();
    });

    const rListener = BookmarkClient.onRemove(
      async (bookmarkId, removeInfo) => {
        const newSidebar = await this.load();
        vnode.state.childrenNodes = newSidebar.top;

        const wasSpaceRemoved =
          vnode.state.spaces.length > newSidebar.spaces.length;
        vnode.state.spaces = newSidebar.spaces;

        if (wasSpaceRemoved) {
          vnode.state.currentSpace = vnode.state.spaces[0];
        }
      }
    );
  }
}

const SidebarService = new _SidebarService();

// --- Exports -----------------------------------

export default SidebarService;
