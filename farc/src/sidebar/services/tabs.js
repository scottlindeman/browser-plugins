import { Logger } from "../../common.js";

import TabClient from "../../clients/tabs.js";

// --- Service -----------------------------------

/**
 * A service to handle user actions with tabs in the sidebar. The main point
 * is this service dispatches actions to the browser apis, and doesn't modify
 * state.
 */
class _TabService {
  /**
   * This will make a tab active. If there is no tabId for the tab,
   * then a new browser tab will be created.
   * In the creation case, we will let the tabCreate event listener
   * set the tab active.
   * An updated farcTab is returned, but can likely be discarded.
   * @param {FarcTab} farcTab The tab to select
   * @return {FarcTab}
   */
  async selectTab(farcTab) {
    if (farcTab.tabId.isEmpty()) {
      const result = await TabClient.createPinnedTab(farcTab);
      return farcTab.withBrowserTab(result);
    } else {
      const result = await TabClient.setActive(farcTab.tabId.get());
      return farcTab;
    }
  }

  /**
   * This will remove a browser tab.
   */
  async removeTab(farcTab) {
    await TabClient.removeTab(farcTab);
  }

  /**
   * Creates a blank new tab.
   */
  async newTab() {
    await TabClient.createFreeTab();
  }
}

const TabService = new _TabService();

export default TabService;
