import { NoneOption, SomeOption, Logger } from "../../common.js";
import { FarcPinnedTab } from "../model.js";

import TabClient from "../../clients/tabs.js";

const LOGGER = new Logger("Helpers");

/**
 * Remap bookmarktab by bookmark id.
 * @param {Array[BrowserBookmarkTab]} bbts A list of stored bookmark tabs from localStorage
 * @return {Object[String: BrowserBookmarkTab]}
 */
const bbtByBookmark = (bbts) => {
  const bookmarkMap = {};
  bbts.forEach((b) => {
    bookmarkMap[b.bookmarkId] = b;
  });
  return bookmarkMap;
};

/**
 * Turn Existing bookmarks and tabs into PinnedTabs.
 * @param {Array[BrowserBookmarkNode]} bookmarkNodes A list of browser bookmark nodes
 * @param {Array[BrowserBookmarkTab]} bbts A list of stored bookmark tabs from localstorage
 * @param {String} pinnedTabType One of FarcConstants.PinnedTabType
 * @return {Array[FarcPinnedTab]}
 */
const bookmarkNodeMapper = async (bookmarkNodes, bbts, pinnedTabType) => {
  const allTabs = await TabClient.list();
  const bbtsMap = bbtByBookmark(bbts);

  return bookmarkNodes.map((n) => {
    let browserTab = NoneOption;
    if (bbtsMap.hasOwnProperty(n.id) && !bbtsMap[n.id].tabId.isEmpty()) {
      const tabIdx = allTabs.findIndex((t) => {
        return t.id === bbtsMap[n.id].tabId.get();
      });

      if (tabIdx >= 0) {
        browserTab = SomeOption(allTabs[tabIdx]);
      }
    }

    return new FarcPinnedTab(browserTab, n, pinnedTabType);
  });
};

// --- Exports -----------------------------------

export { bookmarkNodeMapper };
