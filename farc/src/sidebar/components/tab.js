import m from "mithril";

import { Logger, SomeOption } from "../../common.js";

import { AccordionMenu, AccOpt, AccSec } from "./acc-menu.js";

import TabService from "../services/tabs.js";

import {
  CloseIcon,
  CopyIcon,
  ExpandIcon,
  EggsIcon,
  PlusIcon,
} from "./icons.js";

const LOGGER = new Logger("tab.js");

// --- Class Helpers -----------------------------

const selectedClass = (vnode) => {
  const isActive = vnode.attrs.farcTab.isActive(vnode.attrs.selectedTab);
  return isActive ? ".selected" : "";
};

// --- Components --------------------------------

const TabIcon = {
  view: (vnode) => {
    var imgSrc = vnode.attrs.url;
    if (imgSrc.startsWith("chrome://")) {
      return m(EggsIcon);
    } else {
      if (imgSrc.startsWith("http")) {
        imgSrc = `http://www.google.com/s2/favicons?domain=${imgSrc}`;
      }
      return m("img.farc-tab-icon", {
        width: "16",
        height: "16",
        src: imgSrc,
      });
    }
  },
};

const TabDescription = {
  view: (vnode) => {
    return m(".farc-tab-description", [
      m(TabIcon, { url: vnode.attrs.farcTab.favIconData }),
      m(
        "span",
        { title: vnode.attrs.farcTab.title },
        vnode.attrs.farcTab.title
      ),
    ]);
  },
};

const TabURLCopy = {
  view: (vnode) => {
    return m(
      ".farc-tab-quick-option.url-copy",
      {
        title: "Copy URL",
      },
      m(CopyIcon)
    );
  },
};

const TabExpand = {
  view: (vnode) => {
    return m(
      ".farc-tab-quick-option.expand",
      {
        title: "Expand for options",
        onclick: (e) => {
          e.stopImmediatePropagation();
          vnode.attrs.context.openCloseTray(vnode.attrs.farcTab);
        },
      },
      m(ExpandIcon)
    );
  },
};

const TabDestroy = {
  view: (vnode) => {
    return m(
      ".farc-tab-quick-option.destroy",
      {
        title: "Close the tab",
        onclick: async (e) => {
          e.stopImmediatePropagation();
          await TabService.removeTab(vnode.attrs.farcTab);
        },
      },
      m(CloseIcon)
    );
  },
};

const TabQuickOptions = {
  view: (vnode) => {
    return m(".farc-tab-quick-options", [
      m(TabURLCopy, { url: vnode.attrs.farcTab.url }),
      m(TabExpand, {
        farcTab: vnode.attrs.farcTab,
        context: vnode.attrs.context,
      }),
      m(TabDestroy, { farcTab: vnode.attrs.farcTab }),
    ]);
  },
};

const PinnedTop = {
  view: (vnode) => {
    return m(
      `.tsb-pt-item.tsb-box${selectedClass(vnode)}`,
      {
        title: vnode.attrs.farcTab.title,
        onclick: async () => {
          await vnode.attrs.context.tabSelect(vnode.attrs.farcTab);
        },
      },
      m(TabIcon, { url: vnode.attrs.farcTab.favIconData })
    );
  },
};

const PinnedSpace = {
  view: (vnode) => {
    return m(".farc-tab-wrapper", [
      m(
        `.tsb-tab-item.tsb-box${selectedClass(vnode)}`,
        {
          title: vnode.attrs.farcTab.title, // Perhaps this should be the tab title?
          onclick: async () => {
            await vnode.attrs.context.tabSelect(vnode.attrs.farcTab);
          },
        },
        [
          m(TabDescription, { farcTab: vnode.attrs.farcTab }),
          m(TabQuickOptions, {
            farcTab: vnode.attrs.farcTab,
            context: vnode.attrs.context,
          }),
        ]
      ),
      m(AccordionMenu, {
        openTrayAccId: vnode.attrs.openTrayAccId,
        farcTab: vnode.attrs.farcTab,
        options: [
          AccOpt("Rename", () => {
            LOGGER.info("clicked");
          }),
        ],
      }),
    ]);
  },
};

const TabSpace = {
  view: (vnode) => {
    return m(".farc-tab-wrapper", [
      m(
        `.tsb-tab-item.tsb-box${selectedClass(vnode)}`,
        {
          onclick: async () => {
            await vnode.attrs.context.tabSelect(vnode.attrs.farcTab);
          },
        },
        [
          m(TabDescription, { farcTab: vnode.attrs.farcTab }),
          m(TabQuickOptions, {
            farcTab: vnode.attrs.farcTab,
            context: vnode.attrs.context,
          }),
        ]
      ),
      m(AccordionMenu, {
        openTrayAccId: vnode.attrs.openTrayAccId,
        farcTab: vnode.attrs.farcTab,
        options: [
          AccOpt("Pin tab to space", () => {}),
          AccSec("Move to", [
            AccOpt("H O M E", () => {
              LOGGER.info("clicked 2");
            }),
            AccOpt("C O D E", () => {
              LOGGER.info("nothing happens");
            }),
          ]),
        ],
      }),
    ]);
  },
};

const NewTabSpace = {
  view: (vnode) => {
    return m(
      ".tsb-new-tab.tsb-tab-item.tsb-box",
      {
        onclick: async () => {
          await TabService.newTab();
        },
      },
      m(".farc-tab-description", [m(PlusIcon), m("span", "New Tab")])
    );
  },
};

// --- Exports -----------------------------------

export { PinnedTop, PinnedSpace, TabSpace, NewTabSpace };
