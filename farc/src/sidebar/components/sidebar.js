import m from "mithril";

import { Logger, NoneOption, SomeOption } from "../../common.js";
import MithrilContext from "../../mithril-extensions/context.js";

import SidebarService from "../services/sidebar.js";
import TabService from "../services/tabs.js";

import { PinnedTop } from "./tab.js";
import { Space } from "./space/space.js";

const LOGGER = new Logger("Sidebar");

// --- Context -----------------------------------

class SidebarContext extends MithrilContext {
  async tabSelect(farcTab) {
    this._vnode.state.selectedTab = SomeOption(farcTab);
    this.closeTray();
    await TabService.selectTab(farcTab);
  }

  updateChildrenNodes(newBrowserTab) {
    const idx = this._vnode.state.childrenNodes.findIndex((pt) => {
      return pt.url === newBrowserTab.url;
    });

    const oldTab = this._vnode.state.childrenNodes[idx];
    const newTab = oldTab.withBrowserTab(newBrowserTab);

    this._vnode.state.childrenNodes[idx] = newTab;
    return newTab;
  }

  findAndRemoveTopTab(browserTab) {
    const idx = this._vnode.state.childrenNodes.findIndex((pt) => {
      if (pt.tabId.isEmpty()) {
        return false;
      } else {
        return pt.tabId.get() === browserTab.id;
      }
    });

    if (idx < 0) {
      LOGGER.info(`Top Pinned tab with id ${browserTab.id} not found.`);
      return false;
    }

    const oldTab = this._vnode.state.childrenNodes[idx];
    const newTab = oldTab.removeBrowserTab();

    this._vnode.state.childrenNodes[idx] = newTab;
    return true;
  }

  getChildrenNodes() {
    return this._vnode.state.childrenNodes;
  }

  openCloseTray(farcTab) {
    const currTray = this._vnode.state.openTray;

    let newTray = NoneOption;
    if (currTray.isEmpty() || currTray.get() !== farcTab.accId) {
      newTray = SomeOption(farcTab.accId);
    }

    this._vnode.state.openTray = newTray;
  }

  closeTray() {
    this._vnode.state.openTray = NoneOption;
  }
}

// --- Components --------------------------------

/**
 * This component should control most of the global state
 * of the sidebar application. Try not to add too much global state.
 */
const Sidebar = {
  oninit: async (vnode) => {
    const initialState = await SidebarService.load();
    vnode.state.childrenNodes = initialState.top;
    vnode.state.spaces = initialState.spaces;
    vnode.state.currentSpace = initialState.currentSpace;
    vnode.state.selectedTab = NoneOption;
    vnode.state.openTray = NoneOption;
    SidebarService.addSidebarEventListeners(vnode, m.redraw);
    m.redraw();
  },
  view: (vnode) => {
    const context = new SidebarContext(vnode);

    if (!vnode.state.childrenNodes || !vnode.state.currentSpace) {
      return m(".tsb-main", "Initializing...");
    }

    return m(".tsb-main", [
      m(
        ".tsb-pt-list",
        vnode.state.childrenNodes.map((td) => {
          return m(PinnedTop, {
            context: context,
            farcTab: td,
            selectedTab: vnode.state.selectedTab,
          });
        })
      ),
      m(Space, {
        context: context,
        lite: vnode.state.currentSpace,
        allSpaces: vnode.state.spaces,
        selectedTab: vnode.state.selectedTab,
        openTray: vnode.state.openTray,
      }),
    ]);
  },
};

// --- Exports -----------------------------------

export default Sidebar;
