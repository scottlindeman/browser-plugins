import m from "mithril";

// --- Helpers -----------------------------------

const trayOpenClass = (vnode) => {
  const isOpen =
    vnode.attrs.farcTab.accId === vnode.attrs.openTrayAccId.get("");
  return isOpen ? ".tray-open" : "";
};

// --- DSL ---------------------------------------

/**
 * A singular option for the accordion menu.
 */
const AccOpt = (name, action) => {
  return {
    name,
    action,
  };
};

/**
 * A labelled Accordion Section.
 * This has a number of options inside of it.
 */
const AccSec = (name, options) => {
  return {
    name,
    options,
  };
};

/**
 * Turns a somewhat hierarchical list of options into a flat one.
 * @param {Array[AccOpt | AccSec]} options A list containing opions and sections
 * @return {Array[AccOpt]} A flat list of options.
 */
const toComponents = (options) => {
  const components = [];
  options.forEach((o) => {
    if (o.action !== undefined) {
      components.push(m(AccordionOption, o));
    } else if (o.options !== undefined) {
      components.push(m(AccordionSectionTitle, { name: o.name }));
      o.options.forEach((oo) => {
        components.push(m(AccordionOption, oo));
      });
    }
  });
  return components;
};

// --- Components --------------------------------

const AccordionSectionTitle = {
  view: (vnode) => {
    return m(".farc-accordion-section", m("span", vnode.attrs.name));
  },
};

const AccordionOption = {
  view: (vnode) => {
    return m(
      ".farc-accordion-option",
      {
        title: vnode.attrs.name,
        onclick: () => {
          vnode.attrs.action();
        },
      },
      m("span", vnode.attrs.name)
    );
  },
};

const AccordionMenu = {
  view: (vnode) => {
    return m(
      `#${vnode.attrs.farcTab.accId}.farc-accordion${trayOpenClass(vnode)}`,
      toComponents(vnode.attrs.options)
    );
  },
};

/* --- Exports -------------------------------- */

export { AccordionMenu, AccOpt, AccSec };
