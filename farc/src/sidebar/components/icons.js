import m from "mithril";

/* --- Constants ------------------------------ */

const SMALL_ICON_SIZE = 24;

/* --- Base ----------------------------------- */

const SVGIcon = {
  view: (vnode) => {
    const sqSize =
      vnode.attrs.sqSize === undefined ? SMALL_ICON_SIZE : vnode.attrs.sqSize;

    return m(
      "svg",
      {
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: `${vnode.attrs.minX} ${vnode.attrs.minY} ${sqSize} ${sqSize}`,
        width: sqSize.toString(),
        fill: "currentColor",
        style: `min-width: ${sqSize}px; max-width: ${sqSize}px;`,
      },
      m("path", {
        d: vnode.attrs.d,
      })
    );
  },
};

/* --- Icons ---------------------------------- */

const SpaceIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -2,
      minY: -2,
      d:
        "M2 0h4a2 2 0 0 1 2 2v4a2 2 0 0 1-2 " +
        "2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm12 " +
        "0h4a2 2 0 0 1 2 2v4a2 2 0 0 1-2 2h-4a2 2 0 0 " +
        "1-2-2V2a2 2 0 0 1 2-2zm0 12h4a2 2 0 0 1 2 " +
        "2v4a2 2 0 0 1-2 2h-4a2 2 0 0 1-2-2v-4a2 2 0 0 1 " +
        "2-2zM2 12h4a2 2 0 0 1 2 2v4a2 2 0 0 1-2 " +
        "2H2a2 2 0 0 1-2-2v-4a2 2 0 0 1 2-2z",
    });
  },
};

const CloseIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -6,
      minY: -6,
      d:
        "M7.314 5.9l3.535-3.536A1 1 0 1 0 " +
        "9.435.95L5.899 4.485 2.364.95A1 1 0 1 0 " +
        ".95 2.364l3.535 3.535L.95 9.435a1 1 0 1 0 " +
        "1.414 1.414l3.535-3.535 3.536 3.535a1 1 0 1 0 " +
        "1.414-1.414L7.314 5.899z",
    });
  },
};

const ExpandIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -9,
      minY: -2,
      d:
        "M3 6a3 3 0 1 1 0-6 3 3 0 0 1 0 " +
        "6zm0 14a3 3 0 1 1 0-6 3 3 0 0 1 0 " +
        "6zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z",
    });
  },
};

const CopyIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -5,
      minY: -2,
      d:
        "M5 2v2h4V2H5zm6 0h1a2 2 0 0 1 2 " +
        "2v14a2 2 0 0 1-2 2H2a2 2 0 0 " +
        "1-2-2V4a2 2 0 0 1 2-2h1a2 2 0 0 1 " +
        "2-2h4a2 2 0 0 1 2 2zm0 2a2 2 0 0 1-2 " +
        "2H5a2 2 0 0 1-2-2H2v14h10V4h-1zM4 " +
        "8h6a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 " +
        "5h6a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z",
    });
  },
};

const EggsIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -2,
      minY: -3.5,
      d:
        "M6 17a6 6 0 0 1-6-6c0-3.314 3-9 6-9s6 " +
        "5.686 6 9a6 6 0 0 1-6 6zm6.612-2.161A5.976 " +
        "5.976 0 0 0 14 11c0-2.518-1.732-6.405-3.88-8.127C11.213 " +
        "1.227 12.607 0 14 0c3 0 6 5.686 6 9a6 6 0 0 1-7.388 5.839z",
    });
  },
};

const LChevronIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -4.5,
      minY: -5,
      d:
        "M8.828 7.071l4.95 4.95a1 1 0 1 1-1.414 " +
        "1.414L6.707 7.778a1 1 0 0 1 0-1.414L12.364.707a1 " +
        "1 0 0 1 1.414 1.414l-4.95 4.95zm-6 0l4.95 " +
        "4.95a1 1 0 1 1-1.414 1.414L.707 7.778a1 1 0 0 1 " +
        "0-1.414L6.364.707a1 1 0 1 1 1.414 1.414l-4.95 4.95z",
    });
  },
};

const RChevronIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -5,
      minY: -5,
      d:
        "M11.314 7.071l-4.95-4.95A1 1 0 0 1 " +
        "7.778.707l5.657 5.657a1 1 0 0 1 0 " +
        "1.414l-5.657 5.657a1 1 0 0 1-1.414-1.414l4.95-4.95zm-6 " +
        "0l-4.95-4.95A1 1 0 1 1 1.778.707l5.657 " +
        "5.657a1 1 0 0 1 0 1.414l-5.657 5.657a1 1 0 0 " +
        "1-1.414-1.414l4.95-4.95z",
    });
  },
};

const PlusIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -4.5,
      minY: -4.5,
      d:
        "M8.9 6.9v-5a1 1 0 1 0-2 0v5h-5a1 1 0 1 0 0 " +
        "2h5v5a1 1 0 1 0 2 0v-5h5a1 1 0 1 0 0-2h-5z",
    });
  },
};

const CheckIcon = {
  view: (vnode) => {
    return m(SVGIcon, {
      minX: -5,
      minY: -7,
      d:
        "M5.486 9.73a.997.997 0 0 1-.707-.292L.537 " +
        "5.195A1 1 0 1 1 1.95 3.78l3.535 3.535L11.85.952a1 " +
        "1 0 0 1 1.415 1.414L6.193 9.438a.997.997 0 0 1-.707.292z",
    });
  },
};

/* --- Exports -------------------------------- */

export {
  SpaceIcon,
  CloseIcon,
  ExpandIcon,
  CopyIcon,
  EggsIcon,
  LChevronIcon,
  RChevronIcon,
  PlusIcon,
  CheckIcon,
};
