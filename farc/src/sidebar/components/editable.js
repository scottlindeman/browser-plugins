import m from "mithril";

import Scaffold from "../../mithril-extensions/scaffold.js";

import { CheckIcon, CloseIcon } from "./icons.js";

// --- Extensions --------------------------------

const Scf = Scaffold(
  "initialValue",
  "focused",
  "cancelHandler",
  "submitHandler",
  "stylingId"
);

// --- Component ---------------------------------

const FarcInput = {
  oninit: (vnode) => {
    vnode.state.inputValue = vnode.attrs.initialValue;
  },
  view: (vnode) => {
    Scf.parseAttrs(vnode);
    return m(`${vnode.attrs.stylingId}.farc-input`, [
      m("input.farc-editable", {
        type: "text",
        value: vnode.state.inputValue,
        focus: vnode.attrs.focused,
        onkeyup: (event) => {
          if (event.isComposing || event.keyCode === 229) {
            return;
          } else if (event.key === "Enter") {
            vnode.attrs.submitHandler(vnode.state.inputValue);
          } else {
            vnode.state.inputValue = vnode.dom.childNodes[0].value;
          }
        },
      }),
      m(".farc-input-quick-options", [
        m(
          ".farc-input-quick-option.confirm",
          {
            onclick: () => {
              vnode.attrs.submitHandler(vnode.state.inputValue);
            },
          },
          m(CheckIcon)
        ),
        m(
          ".farc-input-quick-option.destroy",
          {
            onclick: () => {
              vnode.attrs.cancelHandler();
            },
          },
          m(CloseIcon)
        ),
      ]),
    ]);
  },
};

// --- Exports -----------------------------------

export default FarcInput;
