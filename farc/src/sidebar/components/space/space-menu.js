import m from "mithril";

import SpaceService from "./space-service.js";

import { AccordionMenu, AccOpt, AccSec } from "../acc-menu.js";

const SpaceMenu = {
  view: (vnode) => {
    return m(AccordionMenu, {
      farcTab: SpaceService.TopSpaceTab,
      openTrayAccId: vnode.attrs.openTrayAccId,
      options: [
        AccSec("General", [
          AccOpt("Settings", () => {
            browser.runtime.openOptionsPage();
          }),
          AccOpt("New Space", async () => {
            await SpaceService.createNewSpace();
            vnode.attrs.context.closeTray();
          }),
        ]),
        AccSec(vnode.attrs.heavy.title, [
          AccOpt("Rename", () => {
            vnode.attrs.context.closeTray();
            vnode.attrs.context.makeNameEditable();
          }),
          AccOpt("Delete", () => {
            const cMsg = `Delete "${vnode.attrs.heavy.title}"?`;
            if (confirm(cMsg)) {
              // Also needs to remove all tabs under that space
              SpaceService.deleteSpace(vnode.attrs.heavy);
              vnode.attrs.context.closeTray();
            }
          }),
        ]),
        AccSec(
          "Jump to",
          SpaceService.filterThisSpace(
            vnode.attrs.allSpaces,
            vnode.attrs.heavy.bookmarkId
          ).map((s) => {
            return AccOpt(s.title, () => {
              vnode.attrs.context.closeTray();
              vnode.attrs.clickHandler(s);
            });
          })
        ),
      ],
    });
  },
};

// --- Exports -----------------------------------

export default SpaceMenu;
