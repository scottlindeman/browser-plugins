import m from "mithril";

import SpaceService from "./space-service.js";

import { SpaceIcon, ExpandIcon, LChevronIcon, RChevronIcon } from "../icons.js";
import FarcInput from "../editable.js";
import Scaffold from "../../../mithril-extensions/scaffold.js";

// --- Helpers -----------------------------------

/**
 * Removes all spaces from a string.
 * @param {String} name The string with whitespace
 * @return {String} The string without whitespace
 */
const unspacedName = (name) => {
  return name.replace(/\s/g, "");
};

// --- Mithril -----------------------------------

const NameScf = Scaffold("name");

const SwitcherScf = Scaffold("currentBookmarkId", "allSpaces", "context");

const ControlsScf = Scaffold("editName", "heavy", "allSpaces", "context");

// --- Components --------------------------------

const SpaceName = {
  view: (vnode) => {
    NameScf.parseAttrs(vnode);
    return m(".tsb-space-name", { title: vnode.attrs.name }, [
      m(SpaceIcon),
      m("span", unspacedName(vnode.attrs.name)),
    ]);
  },
};

const SpaceSwitcher = {
  view: (vnode) => {
    const currentBookmarkId = vnode.attrs.currentBookmarkId;
    const allSpaces = vnode.attrs.allSpaces.map((v) => {
      return v.node.id;
    });

    const currentIdx = allSpaces.indexOf(currentBookmarkId);

    const backDisabled = currentIdx === 0;
    const forwardDisabled = currentIdx + 1 === allSpaces.length;

    const backOpts = {};
    if (!backDisabled) {
      backOpts.onclick = () => {
        vnode.attrs.context.closeTray();
        vnode.attrs.clickHandler(vnode.attrs.allSpaces[currentIdx - 1]);
      };
    }

    const forwardOpts = {};
    if (!forwardDisabled) {
      forwardOpts.onclick = () => {
        vnode.attrs.context.closeTray();
        vnode.attrs.clickHandler(vnode.attrs.allSpaces[currentIdx + 1]);
      };
    }

    return m(".farc-space-switcher", [
      m(
        `.back.farc-tab-quick-option${backDisabled ? ".disabled" : ""}`,
        backOpts,
        m(LChevronIcon)
      ),
      m(
        ".farc-tab-quick-option.expand",
        {
          onclick: () => {
            vnode.attrs.context.openCloseTray(SpaceService.TopSpaceTab);
          },
        },
        m(ExpandIcon)
      ),
      m(
        `.forward.farc-tab-quick-option${forwardDisabled ? ".disabled" : ""}`,
        forwardOpts,
        m(RChevronIcon)
      ),
    ]);
  },
};

const SpaceControls = {
  view: (vnode) => {
    ControlsScf.parseAttrs(vnode);

    if (vnode.attrs.editName) {
      return m(FarcInput, {
        stylingId: "#space-rename",
        initialValue: vnode.attrs.heavy.title,
        focused: true,
        cancelHandler: () => {
          vnode.attrs.context.cancelEditable();
        },
        submitHandler: (n) => {
          vnode.attrs.context.submitNewName(n);
        },
      });
    } else {
      return m(".space-controls", [
        m(SpaceName, { name: vnode.attrs.heavy.title }),
        m(SpaceSwitcher, {
          currentBookmarkId: vnode.attrs.heavy.bookmarkId,
          allSpaces: vnode.attrs.allSpaces,
          clickHandler: vnode.attrs.clickHandler,
          context: vnode.attrs.context,
        }),
      ]);
    }
  },
};

// --- Exports -----------------------------------

export default SpaceControls;
