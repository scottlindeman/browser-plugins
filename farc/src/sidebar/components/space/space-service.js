import { NoneOption, SomeOption, Logger } from "../../../common.js";
import FarcConstants from "../../../constants.js";

import BookmarkClient from "../../../clients/bookmarks.js";
import StorageClient from "../../../clients/storage.js";
import TabClient from "../../../clients/tabs.js";

import { bookmarkNodeMapper } from "../../services/helpers.js";

import {
  FarcPinnedTab,
  FarcFreeTab,
  FarcSpaceLite,
  FarcSpaceHeavy,
  FarcSidebar,
  FarcSpaceEventListeners,
} from "../../model.js";

const LOGGER = new Logger("SpaceService");

// --- Service Helpers ---------------------------

const DEFAULT_FARC_SPACE = new FarcSpaceHeavy(0, "default", [], []);

// --- Service -----------------------------------

class _SpaceService {
  async setNewSpace(spaceLite) {
    await StorageClient.setCurrentSpace(spaceLite.node.id);
  }

  async createNewSpace() {
    await BookmarkClient.createSpace("new rando space");
  }

  async deleteSpace(heavy) {
    await BookmarkClient.deleteSpace(heavy.bookmarkId);
  }

  async updateSpaceName(newName, heavy) {
    await BookmarkClient.updateSpace(heavy.bookmarkId, {
      title: newName,
    });
  }

  async loadSpace(spaceLite) {
    const bookmarks = await BookmarkClient.getChildrenSpace(spaceLite.node);

    let pinnedFromStorage = [];
    try {
      pinnedFromStorage = await StorageClient.listSpacePinnedTabs(
        spaceLite.node.id
      );
    } catch (error) {
      LOGGER.info("No pinned space tabs in storage");
    }

    const pinned = await bookmarkNodeMapper(
      bookmarks.pinned,
      pinnedFromStorage,
      FarcConstants.PinnedTabType.SPACE
    );

    let freeFromStorage = [];
    try {
      freeFromStorage = await StorageClient.listSpaceFreeTabs(
        spaceLite.node.id
      );
      if (freeFromStorage.length > 0) {
        const allTabs = await TabClient.list();
        freeFromStorage = allTabs
          .filter((t) => {
            return freeFromStorage.indexOf(t.id) >= 0;
          })
          .map((t) => {
            return new FarcFreeTab(t);
          });
      }
    } catch (error) {
      LOGGER.info("No free space tabs in storage");
    }

    const spaceHeavy = new FarcSpaceHeavy(
      spaceLite.node.id,
      spaceLite.title,
      pinned,
      freeFromStorage
    );

    await StorageClient.setSpaceFreeTabs(spaceHeavy);

    return spaceHeavy;
  }

  addSpaceEventListeners(vnode, redraw) {
    const cListener = TabClient.onCreate(async (tab) => {
      let newFarcTab;
      if (tab.discarded && tab.title === FarcConstants.PinnedTabType.SPACE) {
        newFarcTab = vnode.state.heavy.findAndUpdatePinnedTab(tab);
        await StorageClient.setSpacePinnedTabs(vnode.state.heavy);
        await TabClient.setActive(tab.id);
        vnode.attrs.context.tabSelect(newFarcTab);
      } else if (
        tab.discarded &&
        tab.title === FarcConstants.PinnedTabType.TOP
      ) {
        newFarcTab = vnode.attrs.context.updateChildrenNodes(tab);
        await StorageClient.setTopTabs(vnode.attrs.context.getChildrenNodes());
        await TabClient.setActive(tab.id);
        vnode.attrs.context.tabSelect(newFarcTab);
      } else {
        newFarcTab = vnode.state.heavy.addFreeTab(tab);
        await StorageClient.setSpaceFreeTabs(vnode.state.heavy);

        if (tab.active || tab.title === "New Tab") {
          vnode.attrs.context.tabSelect(newFarcTab);
        }
      }

      redraw();
    });
    const rListener = TabClient.onRemove(async (tabId) => {
      const fakeTab = { id: tabId };

      const wasFree = vnode.state.heavy.removeFreeTab(fakeTab);
      if (wasFree) {
        await StorageClient.setSpaceFreeTabs(vnode.state.heavy);
      } else {
        const wasSpacePinned =
          vnode.state.heavy.findAndRemovePinnedTab(fakeTab);
        if (wasSpacePinned) {
          await StorageClient.setSpacePinnedTabs(vnode.state.heavy);
        } else {
          const wasTop = vnode.attrs.context.findAndRemoveTopTab(fakeTab);
          if (wasTop) {
            const children = vnode.attrs.context.getChildrenNodes();
            await StorageClient.setTopTabs(children);
          }
        }
      }

      const newActive = await TabClient.getActive();
      vnode.attrs.context.tabSelect(new FarcFreeTab(newActive));
      redraw();
    });
    const uListener = TabClient.onUpdate((tabId, changeInfo, tab) => {
      vnode.state.heavy.updateFreeTab(tab);
      redraw();
    });

    const buListener = BookmarkClient.onUpdate((bookmarkId, changeInfo) => {
      if (bookmarkId !== vnode.state.heavy.bookmarkId) {
        return;
      }

      vnode.state.heavy = new FarcSpaceHeavy(
        vnode.state.heavy.bookmarkId,
        changeInfo.title,
        vnode.state.heavy.pinnedTabs,
        vnode.state.heavy.freeTabs
      );
      redraw();
    });

    const bcListener = BookmarkClient.onCreate(async (bookmarkId, treeNode) => {
      const isTopLevel = treeNode.parentId === vnode.attrs.lite.node.parentId;
      const isSpace = treeNode.type === "folder";
      if (isTopLevel && isSpace) {
        const newLite = FarcSpaceLite.fromBookmarkNode(treeNode);
        await this.setNewSpace(newLite);
        const newHeavy = await this.loadSpace(newLite);
        vnode.state.heavy = newHeavy;
        vnode.state.fade = ".space-fade-in";
        redraw();

        if (!!vnode.state.listeners) {
          this.removeSpaceEventListeners(vnode.state.listeners);
        }
        vnode.state.listeners = SpaceService.addSpaceEventListeners(
          vnode,
          redraw
        );
        redraw();
      }
    });

    const brListener = BookmarkClient.onRemove(
      async (bookmarkId, removeInfo) => {
        const isTopLevel =
          removeInfo.parentId === vnode.attrs.lite.node.parentId;
        const isSpace = removeInfo.node.type === "folder";
        if (isTopLevel && isSpace) {
          const newLite = vnode.attrs.allSpaces[0];
          await this.setNewSpace(newLite);
          const newHeavy = await this.loadSpace(newLite);
          vnode.state.heavy = newHeavy;
          vnode.state.fade = ".space-fade-in";
          redraw();

          if (!!vnode.state.listeners) {
            this.removeSpaceEventListeners(vnode.state.listeners);
          }
          vnode.state.listeners = SpaceService.addSpaceEventListeners(
            vnode,
            redraw
          );
          redraw();
        }
      }
    );

    return new FarcSpaceEventListeners(
      cListener,
      rListener,
      uListener,
      buListener,
      bcListener,
      brListener
    );
  }

  removeSpaceEventListeners(listeners) {
    TabClient.removeListeners(listeners);
    BookmarkClient.removeListeners(listeners);
  }

  /**
   * Given a list of spaces, return a filtered version that
   * excludes the space with the given bookmarkId
   * @param {Array[SpaceLite]} allSpaces Our list of spaces
   * @param {String} spaceBookmarkId The id to match and exclude
   * @return {Array[SpaceLite]} The filtered list
   */
  filterThisSpace(allSpaces, spaceBookmarkId) {
    return allSpaces.filter((s) => {
      return s.bookmarkId !== spaceBookmarkId;
    });
  }

  /**
   * A mock tab that represents a Space-level
   * menu.
   */
  get TopSpaceTab() {
    return {
      accId: "farc-accordion-space-top",
    };
  }
}

// --- Entrypoint --------------------------------

const SpaceService = new _SpaceService();

export default SpaceService;
