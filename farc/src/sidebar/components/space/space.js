import m from "mithril";

import { Logger } from "../../../common.js";
import MithrilContext from "../../../mithril-extensions/context.js";

import SpaceMenu from "./space-menu.js";
import SpaceControls from "./space-controls.js";
import SpaceService from "./space-service.js";

import { PinnedSpace, TabSpace, NewTabSpace } from "../tab.js";

const LOGGER = new Logger("Space");

// --- Context -----------------------------------

class SpaceContext extends MithrilContext {
  async tabSelect(farcTab) {
    this._vnode.attrs.context.tabSelect(farcTab);
  }

  openCloseTray(farcTab) {
    this._vnode.attrs.context.openCloseTray(farcTab);
  }

  closeTray() {
    this._vnode.attrs.context.closeTray();
  }

  makeNameEditable() {
    this._vnode.state.editName = true;
  }

  cancelEditable() {
    this._vnode.state.editName = false;
  }

  submitNewName(newName) {
    SpaceService.updateSpaceName(newName, this._vnode.state.heavy);
    this._vnode.state.editName = false;
  }
}

// --- Components --------------------------------

const spaceUpdate = (vnode, isInit) => {
  return async (lite) => {
    if (!isInit) {
      vnode.state.fade = ".space-fade-out";
      m.redraw();
    }

    await SpaceService.setNewSpace(lite);
    const heavy = await SpaceService.loadSpace(lite);
    vnode.state.heavy = heavy;
    vnode.state.fade = ".space-fade-in";

    if (!!vnode.state.listeners) {
      SpaceService.removeSpaceEventListeners(vnode.state.listeners);
    }
    vnode.state.listeners = SpaceService.addSpaceEventListeners(
      vnode,
      m.redraw
    );

    m.redraw();
  };
};

const Space = {
  oninit: async (vnode) => {
    vnode.state.editName = false;
    await spaceUpdate(vnode, true)(vnode.attrs.lite);
  },

  view: (vnode) => {
    const switcherClickHandler = spaceUpdate(vnode, false);
    const context = new SpaceContext(vnode);

    if (!vnode.state.heavy) {
      return m("span", "Initializing...");
    }

    const pinnedLength = vnode.state.heavy.pinnedTabs.length;

    return m(`.tsb-space${vnode.state.fade}`, [
      m(SpaceControls, {
        editName: vnode.state.editName,
        heavy: vnode.state.heavy,
        allSpaces: vnode.attrs.allSpaces,
        clickHandler: switcherClickHandler,
        context: context,
      }),
      m(SpaceMenu, {
        openTrayAccId: vnode.attrs.openTray,
        allSpaces: vnode.attrs.allSpaces,
        heavy: vnode.state.heavy,
        clickHandler: switcherClickHandler,
        context: context,
      }),
      m(
        ".tsb-ps-list",
        vnode.state.heavy.pinnedTabs.map((t) => {
          const pattrs = {
            farcTab: t,
            context: context,
            selectedTab: vnode.attrs.selectedTab,
            openTrayAccId: vnode.attrs.openTray,
          };
          return m(PinnedSpace, pattrs);
        })
      ),
      m(
        ".tsb-tab-list",
        [m(NewTabSpace)].concat(
          vnode.state.heavy.freeTabs.map((t) => {
            const tattrs = {
              farcTab: t,
              context: context,
              selectedTab: vnode.attrs.selectedTab,
              openTrayAccId: vnode.attrs.openTray,
            };
            return m(TabSpace, tattrs);
          })
        )
      ),
    ]);
  },
};

// --- Exports -----------------------------------

export { Space };
