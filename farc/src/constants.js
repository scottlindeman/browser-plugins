const FarcConstants = {};

FarcConstants.PinnedTabType = {
  SPACE: "farc-title-space",
  TOP: "farc-title-top",
};

export default FarcConstants;
