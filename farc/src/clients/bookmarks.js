import { Frozen, Logger } from "../common.js";

const LOGGER = new Logger("Bookmarks");

// --- Constants ---------------------------------

const BOOKMARK_DIR = "TSBDATA";

// --- Model -------------------------------------

/** Holds collections of bookmark nodes.
    Pinned nodes are top-level bookmarks.
    Spaces are top-level directories. **/
class TopNodeChildren extends Frozen {
  constructor(pinned, spaces) {
    super({ pinned, spaces });
  }
}

/** Holds a collection of bookmark nodes.
    Pinned nodes are space-level bookmarks.
    In the future we'll add directory support. **/
class SpaceNodeChildren extends Frozen {
  constructor(pinned) {
    super({ pinned });
  }
}

// --- Client ------------------------------------

class _BookmarkClient {
  /** Retrieve the top level bookmark directory
      or create it if it does not exist. **/
  async getOrCreateTop() {
    const bookmarksInput = { title: BOOKMARK_DIR };

    const searchResult = await browser.bookmarks.search(bookmarksInput);

    if (searchResult.length >= 1) {
      return searchResult[0];
    } else {
      return await browser.bookmarks.create(bookmarksInput);
    }
  }

  async getChildrenTop(topnode) {
    const children = await browser.bookmarks.getChildren(topnode.id);

    return new TopNodeChildren(
      children.filter((c) => {
        return c.type === "bookmark";
      }),
      children.filter((c) => {
        return c.type === "folder";
      })
    );
  }

  async getChildrenSpace(spacenode) {
    const children = await browser.bookmarks.getChildren(spacenode.id);

    return new SpaceNodeChildren(
      children.filter((c) => {
        return c.type === "bookmark";
      })
    );
  }

  async createSpace(title) {
    const top = await this.getOrCreateTop();
    const withChildren = await browser.bookmarks.getSubTree(top.id);

    const newSpaceBookmark = {
      title: title,
      index: withChildren[0].children.length,
      parentId: top.id,
      type: "folder",
      url: null,
    };
    return await browser.bookmarks.create(newSpaceBookmark);
  }

  async updateSpace(bookmarkId, updateObj) {
    await browser.bookmarks.update(bookmarkId, updateObj);
  }

  async deleteSpace(bookmarkId) {
    await browser.bookmarks.removeTree(bookmarkId);
  }

  async createPinnedTabInSpace(bookmarkId, url) {
    const newPinnedTabBookmark = {
      url: url,
      index: 0,
      parentId: bookmarkId,
      type: "bookmark",
    };
    return await browser.bookmarks.create(newPinnedTabBookmark);
  }

  onUpdate(callback) {
    browser.bookmarks.onChanged.addListener(callback);
    LOGGER.debug("Added new bookmark onUpdate listener");
    return callback;
  }

  onCreate(callback) {
    browser.bookmarks.onCreated.addListener(callback);
    LOGGER.debug("Added new bookmark onCreate listener");
    return callback;
  }

  onRemove(callback) {
    browser.bookmarks.onRemoved.addListener(callback);
    LOGGER.debug("Added new bookmark onRemove listener");
    return callback;
  }

  removeListeners(listeners) {
    LOGGER.debug("removing listeners");
    browser.bookmarks.onChanged.removeListener(listeners.bookmarkOnUpdate);
    browser.bookmarks.onCreated.removeListener(listeners.bookmarkOnCreate);
    browser.bookmarks.onRemoved.removeListener(listeners.bookmarkOnRemove);
  }
}

// --- Entrypoint --------------------------------

const BookmarkClient = new _BookmarkClient();

export default BookmarkClient;
