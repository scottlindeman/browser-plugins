import { Frozen, NoneOption, SomeOption, Logger } from "../common.js";

const LOGGER = new Logger("Storage");

// --- Storage Keys ------------------------------

const TOP_KEY = "farc-top";

const CURRENT_SPACE_KEY = "farc-current-space";

const SPACE_PINNED_KEY = "farc-space-pinned";

const SPACE_FREE_KEY = "farc-space-free";

const UI_THEME_KEY = "farc-ui-theme";

// --- Model -------------------------------------

/** A representation for pinned tabs. Maps a
    browser (firefox) bookmark to a browser
    (firefox) tab.

    Tabs are optional.
 **/
class BrowserBookmarkTab extends Frozen {
  constructor(bookmarkId, tabId) {
    super({ bookmarkId, tabId });
  }

  static fromLocalStorage(obj) {
    const objTabId = obj.tabId === null ? NoneOption : SomeOption(obj.tabId);

    return new BrowserBookmarkTab(obj.bookmarkId, objTabId);
  }

  toLocalStorage() {
    return {
      bookmarkId: this.bookmarkId,
      tabId: this.tabId.get(null),
    };
  }
}

// --- Client ------------------------------------

class _StorageClient {
  async #listTabs(key) {
    try {
      const results = await browser.storage.local.get(key);
      if (results === undefined || Object.keys(results).length === 0) {
        return {};
      } else {
        return results;
      }
    } catch (error) {
      LOGGER.error(error);
      return {};
    }
  }

  async listTopTabs() {
    const results = await this.#listTabs(TOP_KEY);
    if (Object.keys(results).length === 0) {
      return [];
    }
    return results[TOP_KEY].map((v) => {
      return BrowserBookmarkTab.fromLocalStorage(v);
    });
  }

  async setTopTabs(topTabs) {
    const bbts = topTabs.map((t) => {
      return new BrowserBookmarkTab(t.browserBookmarkNode.id, t.tabId);
    });
    let current = await this.#listTabs(TOP_KEY);
    if (Object.keys(current).length === 0) {
      current = {};
      current[TOP_KEY] = {};
    }

    current[TOP_KEY] = bbts.map((bt) => {
      return bt.toLocalStorage();
    });
    try {
      await browser.storage.local.set(current);
      return true;
    } catch (error) {
      LOGGER.error(e);
      return false;
    }
  }

  async listSpacePinnedTabs(bookmarkId) {
    const results = await this.#listTabs(SPACE_PINNED_KEY);
    if (Object.keys(results).length === 0) {
      return [];
    }
    return results[SPACE_PINNED_KEY][bookmarkId].map((v) => {
      return BrowserBookmarkTab.fromLocalStorage(v);
    });
  }

  async setSpacePinnedTabs(heavy) {
    const bbts = heavy.pinnedTabs.map((t) => {
      return new BrowserBookmarkTab(t.browserBookmarkNode.id, t.tabId);
    });
    let current = await this.#listTabs(SPACE_PINNED_KEY);

    if (Object.keys(current).length === 0) {
      current = {};
      current[SPACE_PINNED_KEY] = {};
    }

    current[SPACE_PINNED_KEY][heavy.bookmarkId] = bbts.map((bt) => {
      return bt.toLocalStorage();
    });
    try {
      await browser.storage.local.set(current);
      return true;
    } catch (error) {
      LOGGER.error(e);
      return false;
    }
  }

  async listSpaceFreeTabs(bookmarkId) {
    const results = await this.#listTabs(SPACE_FREE_KEY);
    if (Object.keys(results).length === 0) {
      return [];
    }
    const bookmarkResult = results[SPACE_FREE_KEY][bookmarkId];
    if (bookmarkResult === undefined) {
      return [];
    } else {
      return bookmarkResult;
    }
  }

  async setSpaceFreeTabs(heavy) {
    const tabIds = heavy.freeTabs.map((t) => {
      return t.tabId.get();
    });
    let current = await this.#listTabs(SPACE_FREE_KEY);

    if (Object.keys(current).length === 0) {
      current = {};
      current[SPACE_FREE_KEY] = {};
    }

    current[SPACE_FREE_KEY][heavy.bookmarkId] = tabIds;
    try {
      await browser.storage.local.set(current);
      return true;
    } catch (e) {
      LOGGER.error(e);
      LOGGER.error(`Could not update freetabs for ${heavy.bookmarkId}`);
      return false;
    }
  }

  async getCurrentSpace() {
    try {
      const results = await browser.storage.local.get(CURRENT_SPACE_KEY);
      if (Object.keys(results).length === 0) {
        return NoneOption;
      } else {
        return SomeOption(results[CURRENT_SPACE_KEY]);
      }
    } catch (error) {
      LOGGER.error(error);
      return NoneOption;
    }
  }

  async setCurrentSpace(bookmarkId) {
    const obj = {};
    obj[CURRENT_SPACE_KEY] = bookmarkId;

    try {
      await browser.storage.local.set(obj);
      return true;
    } catch (error) {
      LOGGER.error(error);
      return false;
    }
  }

  /**
   * Grab the list of UI hex values from storage.
   * @return {Option[Array[String]]} The list of current hex values.
   */
  async getUITheme() {
    try {
      const results = await browser.storage.local.get(UI_THEME_KEY);
      if (Object.keys(results).length === 0) {
        return NoneOption;
      } else {
        return SomeOption(results[UI_THEME_KEY]);
      }
    } catch (error) {
      LOGGER.error(error);
      return NoneOption;
    }
  }

  /**
   * Save the given list of hex values to storage. There should be
   * 8 of them, but this does not gurantee anything.
   * @param {Array[String]} colors The hex values.
   */
  async setUITheme(colors) {
    const obj = {};
    obj[UI_THEME_KEY] = colors;

    try {
      await browser.storage.local.set(obj);
      return true;
    } catch (error) {
      LOGGER.error(error);
      return false;
    }
  }
}

// --- Entrypoint --------------------------------

const StorageClient = new _StorageClient();

export default StorageClient;
