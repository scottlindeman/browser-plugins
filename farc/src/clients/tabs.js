import { Logger } from "../common.js";

const LOGGER = new Logger("TabClient");

// --- Client ------------------------------------

class _TabClient {
  async list() {
    try {
      const tabs = await browser.tabs.query({ currentWindow: true });
      return tabs;
    } catch (error) {
      LOGGER.error(error);
      return [];
    }
  }

  async getActive() {
    try {
      const active = await browser.tabs.query({ active: true });
      LOGGER.info(active);
      return active[0];
    } catch (error) {
      LOGGER.error(error);
      return null; // TODO: I guess use options here
    }
  }

  async setActive(tabId) {
    try {
      await browser.tabs.update(tabId, { active: true });
      return true;
    } catch (error) {
      LOGGER.error(error);
      return false;
    }
  }

  async createFreeTab() {
    const newTab = await browser.tabs.create({});
    return newTab;
  }

  async createPinnedTab(pinnedFarcTab) {
    const newTab = await browser.tabs.create({
      discarded: true,
      title: pinnedFarcTab.pinnedTabType,
      url: pinnedFarcTab.url,
    });
    LOGGER.info(newTab);
    return newTab;
  }

  /**
   * Remove a single tab from the browser.
   * @param {FarcTab} farcTab The tab to remove.
   */
  async removeTab(farcTab) {
    if (!farcTab.tabId.isEmpty()) {
      await browser.tabs.remove(farcTab.tabId.get());
    }
  }

  /**
   * Remove a list of tabs from the browser.
   * @param {Array[FarcTab]} farcTabs The tabs to remove.
   */
  async removeTabs(farcTabs) {
    const tabIds = farcTabs
      .filter((ft) => {
        return !ft.tabId.isEmpty();
      })
      .map((ft) => {
        return ft.tabId.get();
      });

    await browser.tabs.remove(tabIds);
  }

  // Event Subscribers

  /**
     The createCallback takes a tab.
   **/
  onCreate(callback) {
    browser.tabs.onCreated.addListener(callback);
    LOGGER.debug("Added new tab onCreated listener");
    return callback;
  }

  onRemove(callback) {
    browser.tabs.onRemoved.addListener(callback);
    LOGGER.debug("Added new tab onRemoved listener");
    return callback;
  }

  onUpdate(callback) {
    browser.tabs.onUpdated.addListener(callback);
    LOGGER.debug("Added new tab onUpdated listener");
    return callback;
  }

  removeListeners(listeners) {
    LOGGER.debug("removing listeners");
    browser.tabs.onCreated.removeListener(listeners.tabOnCreate);
    browser.tabs.onRemoved.removeListener(listeners.tabOnRemove);
    browser.tabs.onUpdated.removeListener(listeners.tabOnUpdate);
  }
}

// --- Entrypoint --------------------------------

const TabClient = new _TabClient();

export default TabClient;
