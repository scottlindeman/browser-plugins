import assert from "assert";

import { NoneOption, SomeOption } from "../../common.js";
import FarcConstants from "../../constants.js";

import {
  FarcPinnedTab,
  FarcFreeTab,
  FarcSpaceLite,
  FarcSpaceHeavy,
  FarcSidebar,
  FarcSpaceEventListeners,
} from "../../sidebar/model.js";

// --- Fixtures ----------------------------------

const fakeFSH = () => {
  return new FarcSpaceHeavy(134, "Cool Space 1", [], []);
};

const fakeBrowserTab = () => {
  return {
    id: 333,
    title: "A cool new tab",
    url: "https://www.google.com",
    muted: true,
  };
};

const fakeTabInternalTitle = () => {
  return {
    id: 123,
    title: FarcConstants.PinnedTabType.SPACE,
    url: "https://www.google.com/",
    muted: false,
  };
};

const fakeBookmarkNode = () => {
  return {
    id: "mybookmarknode",
    title: "some cool bookmark title",
    url: "a bookmark url",
  };
};

// --- FarcPinnedTab Tests -----------------------

describe("FarcPinnedTab", () => {
  describe("constructor", () => {
    it("should create a FarcPinnedTab with no browserTab", () => {
      const fbn = fakeBookmarkNode();

      const pinned = new FarcPinnedTab(
        NoneOption,
        fbn,
        FarcConstants.PinnedTabType.SPACE
      );

      assert.equal(pinned.browserBookmarkNode, fbn);

      assert.strictEqual(pinned.tabId.isEmpty(), true);

      assert.equal(pinned.defaultTitle, fbn.title);
      assert.equal(pinned.title, fbn.title);

      assert.equal(pinned.defaultUrl, fbn.url);
      assert.equal(pinned.url, fbn.url);

      assert.equal(pinned.pinnedTabType, FarcConstants.PinnedTabType.SPACE);

      assert.strictEqual(pinned.isMuted, false);
    });

    it("should create a FarcPinnedTab with a browserTab", () => {
      const fbn = fakeBookmarkNode();
      const fbt = fakeBrowserTab();

      const pinned = new FarcPinnedTab(SomeOption(fbt), fbn);

      assert.strictEqual(pinned.tabId.isEmpty(), false);
      assert.equal(pinned.tabId.get(), fbt.id);

      assert.equal(pinned.defaultTitle, fbn.title);
      assert.equal(pinned.title, fbt.title);

      assert.equal(pinned.defaultUrl, fbn.url);
      assert.equal(pinned.url, fbt.url);

      assert.strictEqual(pinned.isMuted, true);
    });
  });

  describe("fromBookmarkNode", () => {
    it("should create a FarcPinnedTab with no browserTab", () => {
      const fbn = fakeBookmarkNode();

      const pinned = FarcPinnedTab.fromBookmarkNode(
        fbn,
        FarcConstants.PinnedTabType.TOP
      );

      assert.strictEqual(pinned.tabId.isEmpty(), true);

      assert.equal(pinned.defaultTitle, fbn.title);
      assert.equal(pinned.title, fbn.title);

      assert.equal(pinned.defaultUrl, fbn.url);
      assert.equal(pinned.url, fbn.url);

      assert.equal(pinned.pinnedTabType, FarcConstants.PinnedTabType.TOP);

      assert.strictEqual(pinned.isMuted, false);
    });
  });

  describe("title", () => {
    it("should not display an internal title", () => {
      const fbn = fakeBookmarkNode();
      const ftit = fakeTabInternalTitle();

      const pinned = new FarcPinnedTab(
        SomeOption(ftit),
        fbn,
        FarcConstants.PinnedTabType.SPACE
      );

      assert.notEqual(pinned.title, ftit.title);
      assert.equal(pinned.title, fbn.title);
    });
  });
});

// --- FarcFreeTab Tests -----------------------

describe("FarcFreeTab", () => {
  describe("constructor", () => {
    it("should create a FarcFreeTab with a browserTab", () => {
      const fbt = fakeBrowserTab();

      const free = new FarcFreeTab(fbt);

      assert.strictEqual(free.tabId.isEmpty(), false);
      assert.equal(free.tabId.get(), fbt.id);

      assert.equal(free.defaultTitle, "");
      assert.equal(free.title, fbt.title);

      assert.equal(free.defaultUrl, "");
      assert.equal(free.url, fbt.url);

      assert.strictEqual(free.isMuted, true);
    });
  });
});

// --- FarcSpaceHeavy Tests ----------------------

describe("FarcSpaceHeavy", () => {
  describe("addFreeTab", () => {
    it("should put a new tab on the end of the list", () => {
      const fsh = fakeFSH();
      const fakeNewTab = fakeBrowserTab();

      assert.equal(fsh.freeTabs.length, 0);

      fsh.addFreeTab(fakeNewTab);

      assert.equal(fsh.freeTabs.length, 1);

      const t = fsh.freeTabs[0];

      assert.equal(t.tabId.get(), fakeNewTab.id);
      assert.equal(t.title, fakeNewTab.title);
      assert.equal(t.url, fakeNewTab.url);
      assert.strictEqual(t.isMuted, true);
    });
  });
  describe("removeFreeTab", () => {
    it("should remove a tab from the list", () => {
      const fsh = fakeFSH();
      const tab = fakeBrowserTab();

      fsh.addFreeTab(tab);

      assert.equal(fsh.freeTabs.length, 1);

      fsh.removeFreeTab(tab);

      assert.equal(fsh.freeTabs.length, 0);
    });
    it("should not error when removing a tab that doesn't exist", () => {
      const fsh = fakeFSH();
      const tab = fakeBrowserTab();

      fsh.removeFreeTab(tab);
      assert.equal(fsh.freeTabs.length, 0);
    });
  });
  describe("updateFreeTab", () => {
    it("should update an existing tab with a new title and url", () => {
      const fsh = fakeFSH();
      const tab = fakeBrowserTab();

      const firstTitle = tab.title;
      const firstURL = tab.url;

      fsh.addFreeTab(tab);

      assert.equal(fsh.freeTabs[0].title, firstTitle);
      assert.equal(fsh.freeTabs[0].url, firstURL);

      tab.title = "Something different";
      tab.url = "https://not.here.xyz";

      fsh.updateFreeTab(tab);

      assert.equal(fsh.freeTabs.length, 1);

      const updated = fsh.freeTabs[0];
      assert.notEqual(updated.title, firstTitle);
      assert.notEqual(updated.url, firstURL);
      assert.equal(updated.title, tab.title);
      assert.equal(updated.url, tab.url);
    });

    it("should not error nor update anything if there is no id match", () => {
      const fsh = fakeFSH();
      const tab = fakeBrowserTab();

      const firstTitle = tab.title;
      const firstURL = tab.url;

      fsh.addFreeTab(tab);

      assert.equal(fsh.freeTabs[0].title, firstTitle);
      assert.equal(fsh.freeTabs[0].url, firstURL);

      tab.id = 9347;

      fsh.updateFreeTab(tab);

      assert.equal(fsh.freeTabs.length, 1);

      const updated = fsh.freeTabs[0];
      assert.equal(updated.title, firstTitle);
      assert.equal(updated.url, firstURL);
    });
  });
});
