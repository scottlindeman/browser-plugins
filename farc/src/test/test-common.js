import assert from "assert";

import { NoneOption, SomeOption } from "../common.js";

describe("NoneOption", () => {
  describe("get", () => {
    it("should raise if no default is provided", () => {
      assert.throws(() => {
        NoneOption.get();
      }, Error);
    });
    it("should return the default value if provided", () => {
      const mval = "My Value!";

      const result = NoneOption.get(mval);

      assert.equal(result, mval);
    });
  });

  describe("map", () => {
    it("should do nothing", () => {
      let g = "ONE";
      const updateG = () => {
        g = "TWO";
      };

      NoneOption.map(updateG);

      assert.equal(g, "ONE");
    });

    it("should allow chaining", () => {
      NoneOption.map(() => {
        return 1 + 1;
      }).map(() => {
        return 2 + 2;
      });
      assert.ok(1);
    });
  });

  describe("isEmpty", () => {
    it("should be true", () => {
      assert.equal(NoneOption.isEmpty(), true);
    });
  });
});

describe("SomeOption", () => {
  describe("get", () => {
    it("should return the real value if no default is provided", () => {
      const myval = "sup";
      const so = SomeOption(myval);
      assert.equal(so.get(), myval);
    });
    it("should return the real value even if a default value if provided", () => {
      const mval = "My Value!";

      const so = SomeOption(mval);

      assert.equal(so.get("Not mval"), mval);
    });
  });

  describe("map", () => {
    it("should (unfortunately) allow side effects", () => {
      let g = "ONE";
      const updateG = () => {
        g = "TWO";
      };

      SomeOption(1).map(updateG);

      assert.equal(g, "TWO");
    });

    it("should allow chaining", () => {
      const so = SomeOption(1);
      const end = so
        .map((v) => {
          return v + 1;
        })
        .map((v) => {
          return v + 2;
        });
      assert.equal(end.get(), 4);
    });
  });

  describe("isEmpty", () => {
    it("should be false", () => {
      assert.equal(SomeOption(1).isEmpty(), false);
    });
  });
});
