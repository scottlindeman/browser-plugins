class Frozen {
  constructor(obj) {
    Object.assign(this, obj);
    Object.freeze(Object.seal(this));
  }
}

class _NoneOption extends Frozen {
  get(getDefault = undefined) {
    if (getDefault === undefined) {
      throw new Error("Cannot get value. `undefined` is not allowed.");
    } else {
      return getDefault;
    }
  }

  map(fn) {
    return this;
  }

  isEmpty() {
    return true;
  }
}

const NoneOption = new _NoneOption();

class _SomeOption extends Frozen {
  constructor(val) {
    super({ _val: val });
  }

  get(getDefault = undefined) {
    return this._val;
  }

  map(fn) {
    return SomeOption(fn(this.get()));
  }

  isEmpty() {
    return false;
  }
}

const SomeOption = (val) => {
  return new _SomeOption(val);
};

// --- Logging -----------------------------------

class Logger extends Frozen {
  constructor(context) {
    let loggingLevelString = "DEBUG";
    if (process.env.hasOwnProperty("FARC_LOGGING_LEVEL")) {
      loggingLevelString = process.env["FARC_LOGGING_LEVEL"];
    }

    const levelMap = {
      DEBUG: 4,
      INFO: 3,
      WARN: 2,
      ERROR: 1,
    };

    let loggingLevel = levelMap["DEBUG"];
    if (levelMap.hasOwnProperty(loggingLevelString)) {
      loggingLevel = levelMap[loggingLevelString];
    }

    super({ context, loggingLevel, levelMap });
  }

  debug(...message) {
    if (this.loggingLevel >= this.levelMap["DEBUG"]) {
      console.debug(`[${this.context}]`, ...message);
    }
  }

  info(...message) {
    if (this.loggingLevel >= this.levelMap["INFO"]) {
      console.info(`[${this.context}]`, ...message);
    }
  }

  warn(...message) {
    if (this.loggingLevel >= this.levelMap["WARN"]) {
      console.warn(`[${this.context}]`, ...message);
    }
  }

  error(...message) {
    console.error(`[${this.context}]`, ...message);
  }
}

export { Frozen, NoneOption, SomeOption, Logger };
