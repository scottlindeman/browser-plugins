import m from "mithril";

import { NoneOption, SomeOption } from "../common.js";
import MithrilContext from "../mithril-extensions/context.js";

import ThemeService from "../services/theme.js";

// --- Context -----------------------------------

class OptionsContext extends MithrilContext {
  /**
   * Write the values to the db.
   * @param {Array[String]} selectedTheme The colors of the theme
   */
  async selectTheme(selectedTheme) {
    await ThemeService.setNewTheme(selectedTheme);
    this._vnode.state.currentColors = SomeOption(selectedTheme);
    setSidebarStyle(selectedTheme);
    m.redraw();
  }
}

// --- Helpers -----------------------------------

/**
 * Determine if the the current theme is selected
 * @param {Option[Array[String]]} currentColors A potential list of hex values
 * @param {Array[String]} compareColors A color set to compare with current
 * @return {String} The name of the selected class, if this is the one.
 */
const selectedClass = (currentColors, compareColors) => {
  if (currentColors.isEmpty()) {
    return "";
  } else {
    return currentColors.get().reduce((acc, c, idx) => {
      return acc && c.toLowerCase() === compareColors[idx].toLowerCase();
    }, true)
      ? ".selected"
      : "";
  }
};

/**
 * Update the sidebar html to use the new colors.
 * @param {Array[String]} selectedTheme The colors of the theme
 */
const setSidebarStyle = (selectedTheme) => {
  const sidebarViews = browser.extension.getViews({
    type: "sidebar",
    windowId: browser.windows.getCurrent().id,
  });

  ThemeService.updateSidebarCSS(sidebarViews[0].document, selectedTheme);
};

// --- Components --------------------------------

const Blocks = {
  view: (vnode) => {
    return m(
      ".farc-color-blocks",
      vnode.attrs.themeColors.map((tc) => {
        return m(".farc-color-block", { style: `background-color: ${tc}` });
      })
    );
  },
};

const Colors = {
  view: (vnode) => {
    return m(".farc-options-section", [
      m("h2", "Colors"),
      m(
        "ul.farc-options-list",
        Object.keys(ThemeService.palettes).map((colorName) => {
          const colors = ThemeService.palettes[colorName];
          const sel = selectedClass(vnode.attrs.currentColors, colors);

          return m(
            `li.farc-options-listitem${sel}`,
            {
              onclick: async () => {
                await vnode.attrs.context.selectTheme(colors);
              },
            },
            [
              m("span", colorName.replaceAll("_", " ")),
              m(Blocks, { themeColors: colors }),
            ]
          );
        })
      ),
    ]);
  },
};

const Options = {
  oninit: async (vnode) => {
    vnode.state.currentColors = NoneOption;
    const currentColors = await ThemeService.getCurrentTheme();
    vnode.state.currentColors = SomeOption(currentColors);

    m.redraw();
  },
  view: (vnode) => {
    const context = new OptionsContext(vnode);

    return m(".farc-options-main", [
      m("h1", "Farc Options"),
      m(Colors, {
        currentColors: vnode.state.currentColors,
        context: context,
      }),
    ]);
  },
};

// --- Entrypoint --------------------------------

m.mount(document.body, Options);
