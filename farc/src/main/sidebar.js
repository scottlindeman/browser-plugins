import m from "mithril";

import Sidebar from "../sidebar/components/sidebar.js";

// --- Entrypoint --------------------------------

browser.windows.getCurrent().then((windowInfo) => {
  m.mount(document.body, Sidebar);
});
