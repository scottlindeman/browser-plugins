const dotenv = require("dotenv").config({ path: __dirname + "/.env" });
const path = require("path");
const express = require("express");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const isProduction = process.env.NODE_ENV == "production";

const AppEnv = {};
Object.keys(process.env).forEach((k) => {
  if (k.startsWith("FARC")) {
    AppEnv[k] = JSON.stringify(process.env[k]);
  }
});

const stylesHandler = "style-loader";

const mainDir = "./src/main/";

const config = {
  entry: {
    sidebar: `${mainDir}sidebar.js`,
    options: `${mainDir}options.js`,
  },
  output: {
    path: path.resolve(__dirname, "public/"),
    filename: "[name].js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${mainDir}sidebar.html`,
      filename: "sidebar.html",
      chunk: ["sidebar"],
      excludeChunks: ["options"]
    }),
    new HtmlWebpackPlugin({
      template: `${mainDir}options.html`,
      filename: "options.html",
      chunk: ["options"],
      excludeChunks: ["sidebar"]
    }),
    new webpack.DefinePlugin({
      "process.env": AppEnv
    })

    // Add your plugins here
    // Learn more about plugins from https://webpack.js.org/configuration/plugins/
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/i,
        loader: "babel-loader",
      },
      {
        test: /\.css$/i,
        use: [stylesHandler, "css-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
      },

      // Add your rules for custom modules here
      // Learn more about loaders from https://webpack.js.org/loaders/
    ],
  },
};

module.exports = () => {
  if (isProduction) {
    config.mode = "production";
  } else {
    config.mode = "development";
  }
  return config;
};
