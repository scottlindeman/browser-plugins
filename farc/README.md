# Farc

Mostly for firefox. This is not just a browser extension, but a very specific way of using bookmarks and tabs to provide organization and a new way to interact with tabs. Therefore, it will likely break any existing tabs you already have open.

## Overview

After using the [Arc browser](https://arc.net/) for a week or so, I came to really like how they thought about tabs and organization. I'd like to recreate what I consider the base functionality that works the best.
