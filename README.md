# Browser Extensions

Mostly for firefox.

## Releases
See [#Releases](https://gitlab.com/scottlindeman/browser-plugins/-/releases) to get the latest `.xpi` to install into Firefox.

## Publishing

Run

    > cd browser-plugins/<extension-dir>
    > source ~/.config/firefox/extension-creds
    > npx web-ext sign --api-key=${WEBEXT_KEY} --api-secret=${WEBEXT_SECRET}

## Fonts
The following fonts are not available by default on all systems, but can be added via the following links.

- [Fira Code](https://github.com/tonsky/FiraCode)
- [Fira Mono](https://fonts.google.com/specimen/Fira+Mono)
- [Charter](https://practicaltypography.com/charter.html)
  - **Note:** The otf and ttf files are kinda bottom justified, making them really weird compared to other fonts. MacOS comes with the Charter.ttc file, so it is possible, if you are an insane person, to just find and install the `.ttc` file on Ubuntu.

## Extensions

### Productivity
Change how you interact with sites like Reddit. Remove some of the "popular" pages, to reduce endless scrolling.

### Confluence
Insert CSS to make confluence look nicer. Mostly removes the sans serif font for `georgia` if available on your system and applies consistent styling to links, titles and text.

An [example](https://templates.atlassian.net/wiki/spaces/RD/pages/33189/An+easy+intro+to+using+Confluence) to test against. Here's [another](https://currencenl.atlassian.net/wiki/spaces/IncassomachtigenPD/pages/131563553/Incassomachtigen+Creditor+Implementation+Guidelines+Core+en+B2B+NL).

#### Future
Add the [Hyphenopoly](https://github.com/mnater/Hyphenopoly) library to hyphenate text.
